public class FindDuplicates_AC {
    public static void findDuplicateEmail() {
        List<Lead> leads = new List<Lead>();
        List<Lead> duplicateLeads = new List<Lead>();
        leads.addAll([SELECT ID, Name, Email FROM Lead]);
        Map<String, String> mapEmailName = new Map<String, String>();
        for(Lead leadsList : leads) {
            if(!mapEmailName.containsKey(leadsList.Email)) {
                mapEmailName.put(leadsList.Email, leadsList.Name);
            }
            else {
                duplicateLeads.add(leadsList);
            }
        }
        System.debug('Output : '+duplicateLeads);
    }
}